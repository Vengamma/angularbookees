package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bean.UserDetails;

@Repository
public interface UserDao extends JpaRepository<UserDetails, Integer> {

}
