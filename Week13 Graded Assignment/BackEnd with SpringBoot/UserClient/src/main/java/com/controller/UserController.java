package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bean.UserEntity;
import com.service.UserService;

@RestController
@RequestMapping("/UserActivity")
@CrossOrigin
public class UserController {
	
	@Autowired
	UserService userService;

	@PostMapping(value="signUp",consumes=MediaType.APPLICATION_JSON_VALUE)
	public String signUp(@RequestBody UserEntity user) {
		return userService.signUpUser(user);
	}
	
	@PostMapping(value="signIn",consumes=MediaType.APPLICATION_JSON_VALUE)
	public String signIn(@RequestBody UserEntity user) {
		return userService.signInUser(user);
	}
	
	@GetMapping(value="logout")
	public String logout() {
		return "Logged Out successfully"; 
	}
}
