package com.service;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.UserEntity;
import com.dao.UserDao;

@Service
public class UserService {


	@Autowired
	UserDao userDao;
    
	public String signUpUser(UserEntity user) {
		UserEntity ue = userDao.findByEmailAndPassword(user.getUseremail(), user.getPassword());
		UserEntity ur = userDao.findByEmailOrPassword(user.getUseremail(), user.getPassword());
		if(Objects.nonNull(ue)) {
		   return "Username Already Taken, Try with different one and should be unique";
		  
	   }
		else if(Objects.nonNull(ur)) {
			return"Username Already Taken, Try with different one and should be unique";
		}
	   else {
		   userDao.save(user);
		   return "User Registered Successfully";
	   }
	}
	
	public String signInUser(UserEntity user) {
		UserEntity userent = userDao.findByEmailAndPassword(user.getUseremail(), user.getPassword());
		if(Objects.nonNull(userent)) {
			int len = user.getUseremail().length()-10;
			return "Welcome user "+user.getUseremail().substring(0,len)+" You are Logged IN Successfully ";
		}
		else {
			return "Incorrect Password or Username";
		}
	}
	
}
