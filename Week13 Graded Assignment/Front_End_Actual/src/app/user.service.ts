import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserEntity } from './user-entity';
import { Observable} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(public http:HttpClient) { }

  userSignIn(user:UserEntity):Observable<string>{
    return this.http.post("http://localhost:8282/UserActivity/signIn",user,{responseType:'text'});
  }
  userSignUp(user:UserEntity):Observable<string>{
    return this.http.post("http://localhost:8282/UserActivity/signUp",user,{responseType:'text'});
  }

  getAllUsers():Observable<UserEntity[]>{
    return this.http.get<UserEntity[]>("http://localhost:8181/UserCRUDactivity/retrieveUsers");
  }
  storeUser(user:UserEntity):Observable<string>{
      return this.http.post("http://localhost:8181/UserCRUDactivity/createUser",user,{responseType:'text'});
  }

  deleteUserInfo(uid:number):Observable<string>{
    return this.http.delete("http://localhost:8181/UserCRUDactivity/deleteUser/"+uid,{responseType:'text'});
  }

  updateUserInfo(user:any):Observable<string>{
    return this.http.patch("http://localhost:8181/UserCRUDactivity/updateUser",user,{responseType:'text'});
  }

}
