import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminHomepageComponent } from './admin-homepage/admin-homepage.component';
import { AdminOperationComponent } from './admin-operation/admin-operation.component';
import { UserHomepageComponent } from './user-homepage/user-homepage.component';
import { UserOperationComponent } from './user-operation/user-operation.component';

const routes: Routes = [
  {path:"adminactivity",component:AdminOperationComponent},
  {path:"useractivity",component:UserOperationComponent},
  {path:"adminhome",component:AdminHomepageComponent},
  {path:"userhome",component:UserHomepageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
