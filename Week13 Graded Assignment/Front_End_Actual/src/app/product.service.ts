import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product } from './product';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(public http:HttpClient) { }

  getAllProducts():Observable<Product[]>{
    return this.http.get<Product[]>("http://localhost:8181/ProductCRUD/retrieveProducts");
  }
  storeProductInfo(prod:Product):Observable<string>{
      return this.http.post("http://localhost:8181/ProductCRUD/createProduct",prod,{responseType:'text'});
  }

  deleteProductInfo(pid:number):Observable<string>{
    return this.http.delete("http://localhost:8181/ProductCRUD/deleteProduct/"+pid,{responseType:'text'});
  }

  updateProductInfo(prod:any):Observable<string>{
    return this.http.put("http://localhost:8181/ProductCRUD/updateProduct",prod,{responseType:'text'});
  }


}
