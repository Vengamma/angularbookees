import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import{HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdminOperationComponent } from './admin-operation/admin-operation.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { UserOperationComponent } from './user-operation/user-operation.component';
import { AdminHomepageComponent } from './admin-homepage/admin-homepage.component';
import { UserHomepageComponent } from './user-homepage/user-homepage.component';


@NgModule({
  declarations: [
    AppComponent,AdminOperationComponent, UserOperationComponent, AdminHomepageComponent, UserHomepageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,ReactiveFormsModule,HttpClientModule,FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
